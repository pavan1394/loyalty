const render = (props) => {
  const { children, ...otherProps } = props;
  if (typeof children === 'function') {
    return children(otherProps);
  }
  return children || null;
};

const If = ({ condition, ...props }) => {
  const shouldRender = typeof condition === 'function' ? condition() : condition;

  return shouldRender ? render(props) : null;
};

export default If;
