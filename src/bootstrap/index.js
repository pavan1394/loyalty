import React from 'react';
import {
  Animated,
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Share,
  ImageBackground,
  Button,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import BaseComponent from './BaseComponent';
import DefaultFreebies from '../modules/freebies/components/DefaultFreebies';
import YearlyFreebies from '../modules/freebies/components/YearlyFreebies';
import MonthlyFreebies from '../modules/freebies/components/MonthlyFreebies';
import NextCardStatus from '../modules/loyaltycards/components/NextCardStatus';
import LoyaltyCard from '../modules/loyaltycards/components/LoyaltyCard';
import { connect } from 'react-redux';
import {
  getConfig,
  getUserDetails,
  getNextCardStatus,
  setUserRating,
  getFavourite,
  postReviewAndRate,
  toggleModal,
  getLoyaltyCardsData,
} from '../modules/loyaltycards/loyalty';
import { setPrefixUrl } from '../utils/apiClient';
import If from '../helpers/If';
import Modal from 'react-native-modal';
import StarRating from '../utils/StarRating';

const { height, width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = height * (0.35);
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 43 : 56;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class RewardsPage extends BaseComponent {
  constructor(props) {
    super(props);
    setPrefixUrl(props.prefix);
    this.props.setLoyaltyDetails(props.loyaltyId, props.rewardCardId);
    this.props.getConfig();
    this.props.getUserDetails();
    this.props.getNextCardStatus();
    this.props.getLoyaltyCardsData();

    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      reviewModalOpen: false,
    };
  }

  share = () => {
    Share.share({
      message: this.props.config.shareText,
      title: this.props.config.shareText,
      url: ''
    });
  }

  renderScrollViewContent = (component) => {
    const { config, currentCardId, themeColor } = this.props;
    switch (component) {
      // case 'card':
      //   return <LoyaltyCard navigate={this.navigate} />
      case 'freebies':
        return <DefaultFreebies navigate={this.navigate} />
      case 'yearlyFreebies':
        return <YearlyFreebies navigate={this.navigate} />
      case 'monthlyFreebies':
        return <MonthlyFreebies navigate={this.navigate} />
      case 'saleFreebies':
        return null//<DefaultFreebies navigate={this.navigate} />
      case 'signupFreebies':
        return null//<YearlyFreebies navigate={this.navigate} />
      case 'nextCardStatus':
        if (currentCardId && currentCardId.nextCardId) {
          return (
            <NextCardStatus navigate={this.navigate} displayBtn={true} />
          );
        } else {
          return null;
        }
      case 'benefits':
        return null//<InfoAndTermsPage navigate={this.navigate} />
      case 'termsAndConditions':
        if ((config && config.benefits || '') || (config && config.terms || '')) {
          return (
            <TouchableOpacity
              style={styles.instructionsCard}
              onPress={() => {
                this.navigate('InfoAndTermsPage', {
                  infoData: config && config.benefits || '',
                  termsData: config && config.terms || '',
                  themeColor,
                });
              }}
            >
              <Text style={styles.instructionsTitle}>
                Instructions / Terms & Conditions
              </Text>
              <MaterialIcons name="keyboard-arrow-right" style={[styles.rightArrow, { color: themeColor }]} />
            </TouchableOpacity>
          );
        } else {
          return null;
        }
      default:
        return null;
    }
  }

  render() {
    const { userDetails, favourite, config, prefix, themeColor } = this.props;
    const favouriteColor = favourite ? '#f00' : '#fff';
    const ratingColor = userDetails && userDetails.favorite ? '#f00' : '#fff';

    // Because of content inset the scroll value will be negative on iOS so bring it back to 0.
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );

    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 0, 0],
      extrapolate: 'clamp',
    });

    const titleBgOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, 1],
      extrapolate: 'clamp',
    });

    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 0.9, 0.8],
      extrapolate: 'clamp',
    });

    const cardTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -220],
    });

    return (
      <View style={styles.fill}>

        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true },
          )}
        >
          <If condition={this.props.loyaltyId && this.props.config && this.props.config.components}>
            {() => (
              <View style={styles.scrollViewContent}>
                {this.props.config.components.map(component => {
                  return (
                    <View key={component}>
                      {this.renderScrollViewContent(component)}
                    </View>
                  )
                })}
              </View>
            )}
          </If>
        </Animated.ScrollView>

        <Animated.View
          pointerEvents="none"
          style={[
            styles.header,
            {
              backgroundColor: themeColor,
              transform: [{ translateY: headerTranslate }]
            },
          ]}
        >
          <Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }],
              },
            ]}
            source={require('../images/background_image.png')}
          />
        </Animated.View>

        <Animated.View
          style={[{
            position: 'absolute',
            top: 10,
            right: 20,
          },
          {
            transform: [
              { translateY: cardTranslate }
            ]
          }
          ]}
        >
          <ImageBackground source={require('../images/star.png')} style={styles.ratingImage}>
            <Text style={styles.ratingText}>
              {config && parseFloat(config.rating).toFixed(1) || 4.5}
            </Text>
          </ImageBackground>
        </Animated.View>

        <Animated.View style={[
          styles.actionBar,
          {
            transform: [
              { translateY: cardTranslate }
            ]
          }
        ]}
        >
          <Text style={styles.cardHeadingText}>YOUR REWARD CARD</Text>
          <View style={styles.actionContainerBar}>
            <If condition={prefix && prefix.includes('user')}>
              <TouchableOpacity style={styles.actionContainer} onPress={() => this.props.getFavourite()}>
                <MaterialCommunityIcons name={"heart"} style={{ color: favouriteColor, fontSize: 20 }} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.actionContainer} onPress={() => this.props.toggleModal(true)}>
                <MaterialCommunityIcons name={"star"} style={{ color: ratingColor, fontSize: 20 }} />
              </TouchableOpacity>
            </If>
            <TouchableOpacity style={styles.actionContainer} onPress={() => {
              if (this.props.config) {
                this.share();
              }
            }}>
              <MaterialCommunityIcons name={"share-variant"} style={{ color: '#fff', fontSize: 20 }} />
            </TouchableOpacity>
          </View>
        </Animated.View>

        <Animated.View style={[
          styles.cardContainer,
          {
            transform: [
              // { scale: titleScale },
              { translateY: cardTranslate }
            ]
          }
        ]}
        >
          <LoyaltyCard />
        </Animated.View>

        <Animated.View
          pointerEvents="none"
          style={[
            styles.titleContainer,
            {
              backgroundColor: themeColor,
              opacity: titleBgOpacity,
            }
          ]}
        >
          <Text style={styles.title}>{this.props.title}</Text>
        </Animated.View>

        <Modal isVisible={this.props.openModal}>
          <View style={{ width: '100%', backgroundColor: 'white', padding: 20 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ flex: 1 }}>YOUR RATING</Text>
              <Text>{this.props.userRating}</Text>
            </View>
            <View style={{ width: '80%', height: 60, padding: 10, justifyContent: 'center', alignItems: 'center' }}>
              <StarRating
                style={{ alignSelf: 'center' }}
                rating={parseFloat(this.props.userRating)}
                onRatingChange={userRating => this.props.setUserRating(userRating)}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Button
                title={"Cancel"}
                onPress={() => this.props.toggleModal(false)} />
              <Button
                title={"Rate"}
                onPress={() => this.props.postReviewAndRate()} />
            </View>
          </View>
        </Modal>

      </View>
    );
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';
    const userDetails = state.loyalty.userDetails;
    const loyaltyCards = state.loyalty.loyaltyCards;
    const currentCardId = loyaltyCards.find(item => (userDetails && userDetails.currentCardId) === item.id);

    return {
      config,
      themeColor,
      userDetails,
      currentCardId,
      title: props.title,
      setLoyaltyDetails: props.setLoyaltyDetails,
      loyaltyId: props.loyaltyId,
      rewardCardId: props.rewardCardId,
      openModal: state.loyalty.openModal,
      userRating: state.loyalty.userRating,
      favourite: state.loyalty.favourite,
    };
  },
  {
    getConfig,
    getUserDetails,
    getNextCardStatus, setUserRating,
    setUserRating,
    getFavourite,
    postReviewAndRate,
    toggleModal,
    getLoyaltyCardsData,
  }
)(RewardsPage);

const styles = StyleSheet.create({
  //ScrollView Content
  scrollViewContent: {
    marginTop: height * (0.35 + 0.1),
    minHeight: 1.5 * height,
  },
  commonContainer: {
    margin: 10,
    marginTop: 30,
  },
  raised: {
    elevation: 2,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 1.2,
  },
  cardStatus: {
    margin: 10,
    marginTop: 30,
    borderRadius: 10,
    backgroundColor: '#faf6f7',
    padding: 10,
  },
  instructionsCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
    padding: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    marginTop: 30,
  },
  instructionsTitle: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  rightArrow: {
    // color: '#cb212c',
    fontSize: 30,
  },

  //main container
  fill: {
    flex: 1,
    backgroundColor: '#fff',
  },

  //backgroundImage
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: HEADER_MAX_HEIGHT,
    // backgroundColor: '#cb212c',
    overflow: 'hidden',
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  //rating star
  ratingImage: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
    // resizeMode: 'contain',
  },
  ratingText: {
    fontWeight: 'bold',
    fontSize: 9,
    backgroundColor: 'transparent',
    color: "purple",
    textAlign: 'center'
  },

  //actionBar
  actionBar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    alignSelf: 'center',
    paddingHorizontal: 30,
    position: 'absolute',
    top: height * 0.1,
  },
  cardHeadingText: {
    flex: 0.5,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    backgroundColor: 'transparent'
  },
  actionContainerBar: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionContainer: {
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
  },
  actionImage: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  },

  //card
  cardContainer: {
    alignSelf: 'center',
    position: 'absolute',
    top: height * 0.15,
    overflow: 'hidden',
  },

  //title
  titleContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    minHeight: HEADER_MIN_HEIGHT,
    // backgroundColor: '#cb212c',
    width: '100%',
    justifyContent: 'center'
  },
  title: {
    color: 'white',
    fontSize: 18,
    marginLeft: 10,
  },
});