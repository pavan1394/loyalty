export const data =
  [
    {
      id: '1',
      name: 'Ice Cream',
      points: 500,
    },
    {
      id: '2',
      name: 'Ice Cream',
      points: 500,
    },
    {
      id: '3',
      name: 'Ice Cream',
      points: 500,
    },
    {
      id: '4',
      name: 'Ice Cream',
      points: 500,
    },
    {
      id: '5',
      name: 'Ice Cream',
      points: 500,
    },
    {
      id: '6',
      name: 'Ice Cream',
      points: 500,
    },
  ];

export const monthlyBenefits =
  [
    {
      id: '1',
      image: '',
      title: 'Free Ice Cream',
      body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
      button: "CLAIM NOW",
    },
    {
      id: '2',
      image: '',
      title: 'Free Ice Cream',
      body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
      button: "CLAIM NOW",
    },
  ];

export const footerData =
  [
    {
      id: '1',
      image: './assets/birthday.png',
      title: 'Get 50% OFF On Your Birthday',
      body: 'Celebrate your birthday with your friends & family and make it a memorable experience',
    },
    {
      id: '2',
      image: './assets/anniversary.png',
      title: 'Get 50% OFF On Your Anniversary',
      body: 'Celebrate your anniversary with your friends & family and make it a memorable experience',
    },
  ];

export const allRewardsData =
  [
    {
      id: 1,
      category: 'Unlocked Rewards',
      title: 'Enjoy with no cost, here are your Rewards!',
      rewards: [
        {
          id: 1,
          image: '',
          title: 'Free Ice Cream',
          body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
          points: "100",
          button: "REDEEM NOW",
          lock: false,
        },
      ]
    },
    {
      id: 2,
      category: 'Upcoming Rewards',
      title: 'Unlock these Rewards by Earning Points (OR) Spending',
      rewards: [
        {
          id: 1,
          image: '',
          title: 'Free Ice Cream',
          body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
          points: "100 Points to Unlock",
          button: "BUY NOW",
          lock: true,
        },
        {
          id: 2,
          image: '',
          title: 'Free Ice Cream',
          body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
          points: "100 Points to Unlock",
          button: "BUY NOW",
          lock: true,
        },
        {
          id: 3,
          image: '',
          title: 'Free Ice Cream',
          body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
          points: "100 Points to Unlock",
          button: "BUY NOW",
          lock: true,
        },
        {
          id: 4,
          image: '',
          title: 'Free Ice Cream',
          body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
          points: "100 Points to Unlock",
          button: "BUY NOW",
          lock: true,
        },
        {
          id: 5,
          image: '',
          title: 'Free Ice Cream',
          body: 'Get any 1 Single Scoop Ice Cream of your choice for FREE',
          points: "100 Points to Unlock",
          button: "BUY NOW",
          lock: true,
        },
      ]
    },
  ];

export const infoData = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
export const termsData = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."