import React, { Component } from 'react';
import { NativeModules } from 'react-native';

const EventHandler = NativeModules.EventHandler;

export default class BaseComponent extends Component {

  navigate = (screen, props) => {
    if (this.props.navigate) {
      this.props.navigate(screen, props);
    } else if (this.props.navigation && this.props.navigation.navigate) {
      this.props.navigation.navigate(screen, props);
    } else {
      EventHandler.navigate(screen, props);
    }
  }

  render() {
    return null;
  }
}