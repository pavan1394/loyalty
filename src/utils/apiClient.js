let baseUrl = 'https://www.froogal.in/';
let userUrl = baseUrl + 'user-api/';

let prefix = '';

export const setPrefixUrl = (prefixUrl) => {
  prefix = prefixUrl;
}

const apiClient = {
  Urls: {
    loyalty: '/loyalty/v2/',
    reviewAndRate: userUrl + "review-and-rate",
    toggleFavourite: userUrl + "merchant/toggle-favorite",
  },

  make: function (url, method, params) {
    return fetch(baseUrl + prefix + url, {
      method,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(params),
    }).then(response => response.json());
  },

  post: function (url, params) {
    return this.make(url, 'POST', params);
  },

  get: function (url, params) {
    return this.make(url, 'GET', params);
  },

  mock: function (url, params, mockResponse) {
    return Promise.resolve(mockResponse);
  },
};

export default apiClient;
