import React, {Component} from 'react';
import {
  Image,
  PanResponder,
  View,
} from 'react-native';
import PropTypes from 'prop-types';

const style = {
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  filler: {
    position: 'absolute',
    top: 2,
    bottom: 2,
    left: 0,
    width: 0,
  },
  starWrapper: {
    flex: 1,
    borderWidth: 4,
    borderColor: 'white',
    flexDirection: 'row',
  },
  star: {
    flex: 1,
    alignSelf: 'flex-start',
    //resizeMode: 'center',
    height: 60,
    width: 60,
  }
};

export default class StarRating extends Component {
  static defaultProps = {
    numberOfStars: 5,
    rating: 0,
    backgroundColor: '#BCBCBC',
    fillColor: '#FFD53F',
    onRatingChange: rating => {},
  };

  static propTypes = {
    rating: PropTypes.number,
  };

  constructor(props) {
    super(props);
    this.bound = function (value) {
      return Math.max(0, Math.min(props.numberOfStars, value));
    };
    this.state = {
      width: 0,
    };
  }

  get widthOfOneStar() {
    return this.state.width / this.props.numberOfStars;
  }

  get fillerWidth() {
    return this.props.rating * this.widthOfOneStar;
  }

  getValue(value) {
    return Math.round(value * 2) / 2;
  }

  createPanResponder(index) {
    const truth = () => true;
    return PanResponder.create({
      onStartShouldSetPanResponder: truth,
      onStartShouldSetPanResponderCapture: truth,
      onMoveShouldSetResponderCapture: () => truth,
      onMoveShouldSetPanResponderCapture: () => truth,

      onPanResponderGrant: (e, gestureState) => {
        this.pageX = e.nativeEvent.pageX;
        const locationX = this.locationX = index * this.widthOfOneStar + e.nativeEvent.locationX;
        const value = this.bound(locationX / this.widthOfOneStar);
        this.props.onRatingChange(this.getValue(value));
      },
      onPanResponderMove: (e, gestureState) => {
        const locationX =  this.locationX + gestureState.moveX - this.pageX;
        const value = this.bound(locationX / this.widthOfOneStar);
        this.props.onRatingChange(this.getValue(value));
      },
    });
  }

  onLayoutChange(e) {
    const width = e.nativeEvent.layout.width;
    const left = e.nativeEvent.layout.x;
    this.setState({
      width,
      left
    });
  }

  render() {
    const containerStyle = [
      style.container,
      this.props.style,
      {
        aspectRatio: this.props.numberOfStars,
        backgroundColor: this.props.backgroundColor
      }
    ];
    const fillerStyle = [
      style.filler,
      {
        width: this.fillerWidth,
        backgroundColor: this.props.fillColor
      }
    ];
    return (
      <View onLayout={e => this.onLayoutChange(e)} style={containerStyle}>
        <View style={fillerStyle} />
        {
          [...new Array(this.props.numberOfStars)].map(
            (n, i) => <View style={style.starWrapper} key={i} {...this.createPanResponder(i).panHandlers}>
              <Image style={style.star} aspectRatio={1} source={require('../images/empty.png')}/>
            </View>
          )
        }
      </View>
    );
  }
}
