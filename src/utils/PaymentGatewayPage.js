import React, { Component } from 'react'
import { Alert, Platform, TouchableOpacity, View, WebView } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux';

const bridgeJs = `
  window.Froogal = {
    onTransactionSuccess: function (data) {
      data = JSON.parse(data);
      data.success = true;
      window.postMessage(JSON.stringify(data));
    },
    onTransactionFailure: function (data) {
      data = JSON.parse(data);
      data.success = false;
      window.postMessage(JSON.stringify(data));
    },
  };`;

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
    position: 'relative'
  },
  closeContainer: {
    position: 'absolute',
    right: 0,
    top: Platform.OS === 'ios' ? 20 : 0,
    zIndex: 1,
  },
  closeButton: {
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  closeIcon: {
    color: '#000',
    backgroundColor: 'transparent',
    fontSize: 20
  },
};

class PaymentGatewayPage extends Component {

  static navigatorStyle = {
    navBarHidden: true,
  };

  componentWillMount = () => {
    this.uri = this.props.url;
    this.method = this.props.method;
    this.body = Object
      .entries(this.props.params)
      .map(entry => entry.map(encodeURIComponent).join('='))
      .join('&');
  }

  cancel = () => {
    // TODO: Send a cancel request to server.
    Alert.alert(
      'Cancel',
      'Are you sure you want to cancel the transaction?',
      [
        {
          text: 'Yes', onPress: () => {
            this.props.navigator.pop();
          }
        },
        { text: 'No', style: 'cancel' }
      ]
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.closeContainer}>
          <TouchableOpacity style={styles.closeButton} onPress={() => this.cancel()}>
            <FontAwesome name="close" style={styles.closeIcon} />
          </TouchableOpacity>
        </View>
        <WebView
          source={{
            uri: this.uri,
            method: this.method,
            body: this.body,
          }}
          onMessage={(e) => {
            if (e && e.nativeEvent && e.nativeEvent.data) {
              try {
                const params = JSON.parse(e.nativeEvent.data);
                if (params.success) {
                  this.props.setOrderId(params.id);
                  this.props.newOrderPlaced(params.id);
                  this.props.navigator.push({
                    screen: 'Summary',
                    overrideBackPress: true,
                    title: `Order #${params.id}`,
                    topTabs: [{
                      screenId: 'Tracker',
                      title: 'Tracker',
                    }, {
                      screenId: 'OrderedCart',
                      title: 'Order summary',
                    }],
                  });
                } else {
                  Alert.alert("Transaction Failure", "This transaction has failed. Please try again.");
                  this.props.navigator.pop();
                }
              } catch (e) {
                Alert.alert('Error', `Invalid Data Received. ${e.message}`);
              }
            }
          }}
          injectedJavaScript={bridgeJs}
          bounces={false}
        />
      </View>
    )
  }
}

export default connect(
  state => ({}),
  {
  }
)(PaymentGatewayPage);
