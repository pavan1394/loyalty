import apiClient from "../../../utils/apiClient";;

const GET_CATALOGUE_DETAILS_START = "Modules/rewards/GET_CATALOGUE_DETAILS_START";
const GET_CATALOGUE_DETAILS_SUCCESS = "Modules/rewards/GET_CATALOGUE_DETAILS_SUCCESS";
const ERROR = "Modules/rewards/ERROR";

const getInitialState = () => ({
  categories: [],
  deliveryCharges: [],
  paymentModes: [],
  errorMessage: '',
  loading: false,
  orderMode: null,
  searchEnable: false,
  searchTerm: '',
});

export const getCatalogueData = () => async (dispatch, getState) => {
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, city } = state.home;
  const { orderMode } = state.merchant;

  dispatch({
    type: GET_CATALOGUE_DETAILS_START,
  });

  try {
    const response = await apiClient.post(apiClient.Urls.merchantCatalogue, {
      authToken,
      merchantAccessToken,
      merchantId,
      orderMode,
    });
    console.log('paymentModes', response.paymentModes);
    if (response.success) {
      dispatch({
        type: GET_CATALOGUE_DETAILS_SUCCESS,
        categories: response.categories,
        deliveryCharges: response.deliveryCharges,
        paymentModes: response.paymentModes,
        enableVegOnly: response.enableVegOnly,
      });
    } else {
      dispatch({
        type: GET_CATALOGUE_DETAILS_FAILURE,
        errorMessage: response.message,
      });
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch({
      type: GET_CATALOGUE_DETAILS_FAILURE,
      errorMessage: e.message,
    });
  }
};


export default rewardsReducer = (state = getInitialState(), action) => {
  switch (action.type) {
    case GET_CATALOGUE_DETAILS_START: {
      return {
        ...state,
        errorMessage: '',
        loading: true,
      }
    }
    case GET_CATALOGUE_DETAILS_SUCCESS: {
      return {
        ...state,
        categories: action.categories,
        deliveryCharges: action.deliveryCharges,
        paymentModes: action.paymentModes,
        loading: false,
        enableVegOnly: action.enableVegOnly,
      }
    }
    case GET_CATALOGUE_DETAILS_FAILURE: {
      return {
        ...state,
        errorMessage: action.errorMessage,
        loading: false,
      }
    }
    case SELECT_ORDER_MODE: {
      return {
        ...state,
        orderMode: action.orderMode,
      }
    }
    case TOGGLE_SEARCH: {
      return {
        ...state,
        searchEnable: action.searchEnable,
      }
    }
    case SEARCH_DATA: {
      return {
        ...state,
        searchTerm: action.searchTerm,
      }
    }
    case CLEAR_SEARCH_DATA: {
      return {
        ...state,
        searchTerm: '',
      }
    }
    default:
      return state;
  }
};