import apiClient from "../../utils/apiClient";
import { getFreebiesData, getCanBuyFreebiesData } from "react-native-loyalty/src/modules/freebies/freebies";
import { Alert } from 'react-native';

const GET_LOYALTY_CARDS_DATA_START = "Modules/loyalty/GET_LOYALTY_CARDS_DATA_START";
const GET_LOYALTY_CARD_DATA_SUCCESS = "Modules/loyalty/GET_LOYALTY_CARD_DATA_SUCCESS";
const GET_LOYALTY_CONFIG_START = "Modules/loyalty/GET_LOYALTY_CONFIG_START";
const GET_LOYALTY_CONFIG_SUCCESS = "Modules/loyalty/GET_LOYALTY_CONFIG_SUCCESS";
const GET_USER_DETAILS_START = "Modules/loyalty/GET_USER_DETAILS_START";
const GET_USER_DETAILS_SUCCESS = "Modules/loyalty/GET_USER_DETAILS_SUCCESS";
const SET_LOYALTY_CARD_ID = "Modules/loyalty/SET_LOYALTY_CARD_ID";
const APPLY_LOYALTY_CARD = "Modules/loyalty/APPLY_LOYALTY_CARD";
const GET_NEXT_CARD_STATUS_START = "Modules/loyalty/GET_NEXT_CARD_STATUS_START";
const GET_NEXT_CARD_STATUS_SUCCESS = "Modules/loyalty/GET_NEXT_CARD_STATUS_SUCCESS";
const ERROR = "Modules/loyalty/ERROR";

const getInitialState = () => ({
  loyaltyCards: [],
  config: null,
  loading: false,
  error: {
    title: '',
    message: '',
  },
  userDetails: null,
  loyaltyCardId: null,
  shouldApply: null,
  nextCardStatusDetails: null,
});

export const getConfig = () => async (dispatch, getState) => {
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, loyaltyId, rewardCardId } = state.home;

  let url = apiClient.Urls.loyalty + loyaltyId + '/config/';
  console.log('URL', url, authToken, merchantAccessToken, merchantId)

  dispatch({
    type: GET_LOYALTY_CONFIG_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
      merchantId,
      loyaltyId,
    });
    console.log('config', response);
    if (response.success) {
      dispatch({
        type: GET_LOYALTY_CONFIG_SUCCESS,
        config: response,
      });
    } else {
      dispatch(displayError('', response.message));
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', e.message));
  }
};

export const getUserDetails = () => async (dispatch, getState) => {
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, loyaltyId, rewardCardId } = state.home;

  let url = apiClient.Urls.loyalty + loyaltyId + '/user/details';

  dispatch({
    type: GET_USER_DETAILS_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
      merchantId,
      loyaltyId,
    });
    console.log('user', response);
    if (response.success) {
      dispatch(setLoyaltyCardId(response.currentCardId));
      dispatch({
        type: GET_USER_DETAILS_SUCCESS,
        userDetails: response,
      });
    } else {
      dispatch(displayError('', response.message));
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', e.message));
  }
};

export const getLoyaltyCardsData = () => async (dispatch, getState) => {
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, loyaltyId, rewardCardId } = state.home;
  let url = apiClient.Urls.loyalty + loyaltyId + '/cards/';

  dispatch({
    type: GET_LOYALTY_CARDS_DATA_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
      merchantId,
      loyaltyId,
    });
    console.log('cards', response);
    if (response.success) {
      dispatch({
        type: GET_LOYALTY_CARD_DATA_SUCCESS,
        loyaltyCards: response.cards,
      });
    } else {
      dispatch(displayError('', response.message));
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', e.message));
  }
};

const displayError = (title, message, loading = 'loading') => ({
  type: ERROR,
  title,
  message,
  loading,
});

export const setLoyaltyCardId = (loyaltyCardId) => async (dispatch, getState) => {
  dispatch({
    type: SET_LOYALTY_CARD_ID,
    loyaltyCardId,
  });
  dispatch(getFreebiesData('base'));
  dispatch(getFreebiesData('monthly'));
  dispatch(getFreebiesData('yearly'));
};

export const applyLoyaltyCard = () => async (dispatch, getState) => {
  const state = getState();
  let url = apiClient.Urls.loyalty + loyaltyId + '/apply';
  const response = await apiClient.post(url, {
    authToken: state.session.authToken,
    merchantId: state.home.merchantId,
    html: false,
  });
  if (response.success) {
    dispatch({
      type: APPLY_LOYALTY_CARD,
      shouldApply: false,
    });
  } else {
    Alert.alert('Failed!', response.message || 'Something went wrong');
  }
}

export const getNextCardStatus = () => async (dispatch, getState) => {
  const state = getState();
  const { loyaltyId } = state.home;
  const { authToken, merchantAccessToken } = state.session;
  let url = apiClient.Urls.loyalty + loyaltyId + '/next-card-status/';

  dispatch({
    type: GET_NEXT_CARD_STATUS_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
    });
    console.log('getNextCardStatus', response);

    if (response.success) {
      dispatch({
        type: GET_NEXT_CARD_STATUS_SUCCESS,
        nextCardStatusDetails: {
          percent: parseInt(response.percent) / 100,
          text: response.text,
        }
      });
    } else {
      dispatch(displayError('', response.message));
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', e.message));
  }
}

export default loyaltyReducer = (state = getInitialState(), action) => {
  switch (action.type) {
    case ERROR: {
      return {
        ...state,
        [action.loading]: false,
        error: {
          title: action.title,
          message: action.message,
        }
      }
    }
    case GET_LOYALTY_CARDS_DATA_START: {
      return {
        ...state,
        loading: true,
      }
    }
    case GET_LOYALTY_CARD_DATA_SUCCESS: {
      return {
        ...state,
        loading: false,
        loyaltyCards: action.loyaltyCards
      }
    }
    case GET_LOYALTY_CONFIG_START: {
      return {
        ...state,
        loading: true,
      }
    }
    case GET_LOYALTY_CONFIG_SUCCESS: {
      return {
        ...state,
        loading: false,
        config: action.config
      }
    }
    case GET_USER_DETAILS_SUCCESS: {
      return {
        ...state,
        loading: false,
        userDetails: action.userDetails
      }
    }
    case SET_LOYALTY_CARD_ID: {
      return {
        ...state,
        loyaltyCardId: action.loyaltyCardId
      }
    }
    case APPLY_LOYALTY_CARD: {
      return {
        ...state,
        shouldApply: action.shouldApply,
      }
    }
    case GET_NEXT_CARD_STATUS_START: {
      return {
        ...state,
        loading: true,
      }
    }
    case GET_NEXT_CARD_STATUS_SUCCESS: {
      return {
        ...state,
        loading: false,
        nextCardStatusDetails: action.nextCardStatusDetails,
      }
    }
    default:
      return state;
  }
};