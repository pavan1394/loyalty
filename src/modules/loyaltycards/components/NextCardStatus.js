import React from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
} from 'react-native';
import * as Progress from 'react-native-progress';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import BaseComponent from '../../../bootstrap/BaseComponent';
import If from '../../../helpers/If';
import { connect } from 'react-redux';

class NextCardStatus extends BaseComponent {
  render() {
    const { data, themeColor } = this.props;
    return (
      <View style={[styles.flexOne, styles.cardStatus, styles.raised]}>
        <View style={styles.header}>
          <MaterialIcons name={"star"} style={styles.starIcon} />
          <Text style={styles.headerText}>YOUR VIP CARD STATUS</Text>
        </View>
        <Progress.Bar
          progress={data && data.percent || 0}
          width={null}
          height={30}
          borderRadius={20}
          color={'#ffba3f'}
          unfilledColor={'#553821'}
          borderWidth={0}
        />
        <Text style={styles.footerText}>{data && data.text || ''}</Text>
        <If condition={this.props.displayBtn}>
          <View style={styles.footerBtn}>
            <Button
              title={"Get me in!"}
              color={themeColor}
              onPress={() => {
                this.navigate('NextCardPage', {
                  navigate: this.navigate,
                  themeColor,
                });
              }}
            />
          </View>
        </If>
      </View>
    )
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';

    return {
      themeColor,
      data: state.loyalty.nextCardStatusDetails,
    }
  }
)(NextCardStatus);

const styles = StyleSheet.create({
  flexOne: {
    flex: 1
  },
  cardStatus: {
    margin: 10,
    marginTop: 30,
    borderRadius: 10,
    backgroundColor: '#faf6f7',
    padding: 10,
  },
  raised: {
    elevation: 2,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 1.2,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 10,
  },
  starIcon: {
    color: '#ffba3f',
    fontSize: 20,
  },
  headerText: {
    color: '#553821',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
  },
  footerText: {
    color: '#553821',
    marginTop: 10,
  },
  footerBtn: {
    alignSelf: 'flex-end',
    marginTop: 10,
  }
});