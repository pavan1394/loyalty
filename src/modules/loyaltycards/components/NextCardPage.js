import React from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LoyaltyCard from './LoyaltyCard';
import DefaultFreebies from '../../freebies/components/DefaultFreebies';
import YearlyFreebies from '../../freebies/components/YearlyFreebies';
import MonthlyFreebies from '../../freebies/components/MonthlyFreebies';
import NextCardStatus from './NextCardStatus';
import BaseComponent from '../../../bootstrap/BaseComponent';

export default class NextCardPage extends BaseComponent {
  render() {
    const { themeColor } = this.props;
    return (
      <ScrollView style={styles.fill}>
        <NextCardStatus navigate={this.navigate} displayBtn={false} />
        <View style={{ marginTop: 30 }}>
          <LoyaltyCard />
        </View>
        <DefaultFreebies navigate={this.navigate} />
        <YearlyFreebies navigate={this.navigate} />
        <MonthlyFreebies navigate={this.navigate} />
        <TouchableOpacity
          style={styles.instructionsCard}
          onPress={() => {
            this.navigate('InfoAndTermsPage', {});
          }}
        >
          <Text style={styles.instructionsTitle}>
            Instructions / Terms & Conditions
            </Text>
          <MaterialIcons name="keyboard-arrow-right" style={[styles.rightArrow, { color: themeColor }]} />
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor: '#fff',
  },
  instructionsCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
    padding: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    marginTop: 30,
  },
  instructionsTitle: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  rightArrow: {
    // color: '#cb212c',
    fontSize: 30,
  },
});