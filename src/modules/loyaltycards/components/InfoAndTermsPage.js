import React from 'react';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import BaseComponent from '../../../bootstrap/BaseComponent';
import If from 'react-native-loyalty/src/helpers/If';

export default class InfoAndTermsPage extends BaseComponent {
  render() {
    const { infoData, termsData, themeColor } = this.props;
    return (
      <View style={styles.mainContainer}>
        <If condition={infoData}>
          <View>
            <View style={styles.titleContainer}>
              <MaterialIcons name={'info'} style={[styles.icon, { color: themeColor }]} />
              <Text style={styles.title}>Instructions & Info</Text>
            </View>
            <Text style={styles.data}>{infoData}</Text>
          </View>
        </If>
        <If condition={termsData}>
          <View>
            <View style={styles.titleContainer}>
              <MaterialIcons name={'library-books'} style={[styles.icon, { color: themeColor }]} />
              <Text style={styles.title}>Terms & Conditions</Text>
            </View>
            <Text style={styles.data}>{termsData}</Text>
          </View>
        </If>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
    backgroundColor: '#fff',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  icon: {
    // color: '#cb212c',
    fontSize: 20,
    marginRight: 5,
  },
  title: {
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
  },
  data: {
    color: '#000',
  },
});