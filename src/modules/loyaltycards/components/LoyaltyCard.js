import React from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import { connect } from "react-redux";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import BaseComponent from '../../../bootstrap/BaseComponent';
import { applyLoyaltyCard } from 'react-native-loyalty/src/modules/loyaltycards/loyalty';
import Swiper from 'react-native-swiper';
import { Button } from 'native-base'

const { height, width } = Dimensions.get('window');

class LoyaltyCard extends BaseComponent {

  render() {
    const { userDetails, cardsToDisplay, themeColor } = this.props;

    if (!this.props.loyaltyId) {
      return (
        <View style={[styles.card, { backgroundColor: themeColor }]}>
          <View style={styles.lockOverlay}>
            <MaterialIcons name={"lock"} style={styles.lockIcon} />
          </View>
          <ImageBackground
            source={require('../../../images/card_stars.png')}
            style={[styles.cardBgImage]}
          >
            <View style={styles.cardContent}>
              <View style={styles.cardRightContent}>
                <Image source={require('../../../images/cs_logo.png')} style={styles.logo} />
                <Text style={styles.cardType}>
                  {'This outlet doesnot offer loyalty'}
                </Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      )
    } else if (userDetails && userDetails.canApply) {
      return (
        <View>
          <Swiper containerStyle={[styles.card, { backgroundColor: themeColor }]}>
            {cardsToDisplay.map((card, index) => {
              return (
                <View key={index} style={[styles.card, { backgroundColor: themeColor }]}>
                  <View style={styles.lockOverlay}>
                    <MaterialIcons name={"lock"} style={styles.lockIcon} />
                  </View>
                  <ImageBackground
                    source={card.backgroundImage ? { uri: card.backgroundImage } : require('../../../images/card_stars.png')}
                    style={[styles.cardBgImage]}
                  >
                    <View style={styles.cardContent}>
                      <View style={styles.cardRightContent}>
                        <Image source={require('../../../images/cs_logo.png')} style={styles.logo} />
                        <Text style={styles.cardType}>{card.name}</Text>
                      </View>
                    </View>
                  </ImageBackground>
                </View>
              );
            })}
          </Swiper>
          <Button style={[styles.button]} onPress={() => this.props.applyLoyaltyCard()}>
            <Text style={styles.applyButtonText}>APPLY NOW</Text>
          </Button>
        </View>
      );
    } else {
      return (
        <Swiper containerStyle={[styles.card, { backgroundColor: themeColor }]}>
          {cardsToDisplay.map((card, index) => {
            return (
              <View key={index} style={[styles.card, { backgroundColor: themeColor }]}>
                {userDetails && userDetails.canApply ?
                  <View style={styles.lockOverlay}>
                    <MaterialIcons name={"lock"} style={styles.lockIcon} />
                  </View> : null
                }
                <ImageBackground
                  source={card.backgroundImage ? { uri: card.backgroundImage } : require('../../../images/card_stars.png')}
                  style={[styles.cardBgImage]}
                >
                  <View style={styles.cardContent}>
                    <View style={styles.cardLeftContent}>
                      <Text style={styles.textAroundPoints}>{'YOU HAVE'}</Text>
                      <Text style={styles.pointsText}>{userDetails && userDetails.points || 0}</Text>
                      <Text style={styles.textAroundPoints}>{'POINTS'}</Text>
                    </View>
                    <View style={styles.cardRightContent}>
                      <Image source={require('../../../images/cs_logo.png')} style={styles.logo} />
                      <Text style={styles.cardType}>{card.name}</Text>
                    </View>
                  </View>
                </ImageBackground>
              </View>
            );
          })}
        </Swiper>
      )
    }
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';
    const userDetails = state.loyalty.userDetails;
    const loyaltyCards = state.loyalty.loyaltyCards;
    const cardsToDisplay = loyaltyCards.filter(item => (userDetails && userDetails.currentCardId) === item.id);

    return {
      themeColor,
      userDetails,
      cardsToDisplay,
      shouldApply: state.loyalty.shouldApply,
      appliedTextTop: state.loyalty.appliedTextTop,
      verificationCode: state.loyalty.verificationCode,
      appliedTextBottom: state.loyalty.appliedTextBottom,
      loyaltyId: state.home.loyaltyId,
    }
  },
  {
    applyLoyaltyCard,
  }
)(LoyaltyCard);

const styles = StyleSheet.create({
  card: {
    width: width * 0.9,
    height: width * 0.55,
    borderRadius: 20,
    alignSelf: 'center',
    overflow: 'hidden',
    // backgroundColor: '#cb212c',
  },
  lockOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    zIndex: 999,
  },
  lockIcon: {
    color: '#000',
    fontSize: 50,
    marginBottom: 5,
  },
  cardBgImage: {
    flex: 1,
    alignSelf: 'stretch',
    height: '100%',
    width: '100%',
    padding: 30,
    // resizeMode: 'cover',
  },
  cardContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardLeftContent: {
    flexDirection: 'column',
  },
  textAroundPoints: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 12,
    textAlign: 'center'
  },
  pointsText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22,
    textAlign: 'center'
  },
  cardRightContent: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  logo: {
    borderRadius: 10,
  },
  cardType: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: '#e03e23',
    borderRadius: 8,
    elevation: 0,
    alignSelf: 'center',
    marginTop: 20
  },
  applyButtonText: {
    color: 'white',
    fontWeight: 'bold',
    paddingHorizontal: 20
  },
});