import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Clipboard,
  ToastAndroid,
  Dimensions,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Card } from 'native-base';
import BaseComponent from '../../../bootstrap/BaseComponent';
import { connect } from 'react-redux';
import If from '../../../helpers/If';
import { buyFreebies } from '../freebies';

const { height, width } = Dimensions.get('window');

class FreebiePage extends BaseComponent {

  onPress = () => {
    ToastAndroid.show('Long press to copy Voucher Code!', ToastAndroid.SHORT);
  }

  writeToClipboard = async (couponCode) => {
    await Clipboard.setString(couponCode);
    ToastAndroid.show('Copied to Clipboard!', ToastAndroid.SHORT);
  };

  goToPaymentGateway = (params) => {
    this.navigate('PaymentGatewayPage', { navigate: this.navigate, params });
  };

  render() {
    const { item, config, userDetails, lockCheck, deficitPoints, themeColor } = this.props;
    return (
      <View style={styles.fill}>
        <ScrollView contentContainerStyle={styles.contentContainerStyle}>

          <ImageBackground
            source={item.image ? { uri: item.image } : require('../../../images/freebie_bg.png')}
            style={styles.backgroundImage}
          >
            <View style={styles.imageFiller} />
            <Image source={require('../../../images/cs_logo.png')} style={styles.logo} />
          </ImageBackground>

          <Card style={styles.freebieCard}>
            <If condition={item.name}>
              <Text style={{ color: themeColor, fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>{item.name}</Text>
            </If>
            <If condition={item.description}>
              <Text style={{ color: '#000000', fontSize: 16, textAlign: 'center' }}>{item.description}</Text>
            </If>
            <If condition={item.worth}>
              <Text style={{ color: themeColor, fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>
                {parseInt(item.worth)} <Text style={{ color: themeColor, fontWeight: 'bold', fontSize: 16 }}>Points</Text>
              </Text>
            </If>
          </Card>

          <View style={{ marginTop: (height * 0.1) + 15 }}>
            {
              !this.props.freebie ?
                (<Card style={styles.containerCard}>
                  <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <Text style={{ color: '#000000', fontSize: 18, textAlign: 'center', marginRight: 5 }}>Availabe Points:</Text>
                    <Text style={{ color: themeColor, fontWeight: 'bold', fontSize: 18, textAlign: 'center' }}>
                      {userDetails && userDetails.points || 0} <Text style={{ color: themeColor, fontWeight: 'bold', fontSize: 16 }}>Points</Text>
                    </Text>
                  </View>
                  <Text style={{ color: '#000000', fontWeight: 'bold', fontSize: 18, textAlign: 'center' }}>
                    {lockCheck ?
                      "You can redeem this Freebie" :
                      `You need ${deficitPoints} more points to UNLOCK this Freebie`
                    }
                  </Text>
                </Card>) :
                (
                  <If condition={this.props.freebie && this.props.freebie.freebieItem.couponCode}>
                    {() => (
                      <Card style={styles.containerCard}>
                        <View style={{ flexDirection: 'column', padding: 20 }}>
                          {/* <Text style={{ color: '#000000', fontSize: 16, textAlign: 'center', marginBottom: 10 }}>Use this Voucher Code</Text> */}
                          <TouchableOpacity
                            onPress={() => this.onPress()}
                            onLongPress={() => this.writeToClipboard(this.props.freebie ? this.props.freebie.freebieItem.couponCode : '')}
                            style={{ borderWidth: 0.5, borderColor: '#000', borderRadius: 10, borderStyle: 'dashed', padding: 20 }}
                          >
                            <Text style={{ color: '#000000', fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>{this.props.freebie && this.props.freebie.freebieItem.couponCode}</Text>
                          </TouchableOpacity>
                        </View>
                        <If condition={this.props.freebie && this.props.freebie.freebieItem.qrCode}>
                          {() => (
                            <View style={{ flexDirection: 'column', padding: 20 }}>
                              {/* <Text style={{ color: '#000000', fontSize: 16, textAlign: 'center', marginBottom: 10 }}>Show this QR Code to the Merchant</Text> */}
                              <Image source={{ uri: this.props.freebie.freebieItem.qrCode }} style={{ height: 140, width: 140, alignSelf: 'center' }} />
                            </View>
                          )}
                        </If>
                      </Card>
                    )}
                  </If>
                )
            }

            <Card style={[styles.containerCard, styles.availability]}>
              <Text style={{ color: themeColor, fontWeight: 'bold', fontSize: 16, textAlign: 'left' }}>Availability:</Text>
              <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                {['M', 'Tu', 'W', 'T', 'F', 'Sa', 'S'].map((o, i) => {
                  return (
                    <View key={i} style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                      <Text style={{ fontSize: 20, color: '#333' }}>{o}</Text>
                      <Image
                        source={item.validDays[i] ? require('../../../images/yes.png') : require('../../../images/no.png')}
                        style={{ height: 30, width: 30 }}
                      />
                    </View>
                  );
                })}
              </View>
              <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 16, textAlign: 'left' }}>
                Min Bill. {config.currency}{item.minimumBillAmount}
              </Text>
            </Card>

            <If condition={item.terms}>
              {() => (
                <Card style={styles.instructionsCard}>
                  <TouchableOpacity
                    style={styles.instructions}
                    onPress={() => {
                      this.navigate('InfoAndTermsPage', {
                        infoData: '',
                        termsData: item.terms,
                        themeColor,
                      });
                    }}
                  >
                    <Text style={{ color: '#000000', fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>
                      Instructions / Terms & Conditions
                  </Text>
                    <MaterialIcons name="keyboard-arrow-right" style={{ color: themeColor, fontSize: 30 }} />
                  </TouchableOpacity>
                </Card>
              )}
            </If>
          </View>

        </ScrollView>
        <If condition={!this.props.freebie}>
          {() => (
            <TouchableOpacity onPress={() => {
              this.props.buyFreebies(this.goToPaymentGateway);
            }} style={[styles.buyCard, { backgroundColor: themeColor }]}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>
                {"GET THIS FREEBIE"}
              </Text>
            </TouchableOpacity>
          )}
        </If>
      </View>
    );
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';

    return {
      config,
      themeColor,
      userDetails: state.loyalty.userDetails,
      freebie: state.freebies.freebie,
    }
  },
  {
    buyFreebies
  }
)(FreebiePage);

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  contentContainerStyle: {
    flexDirection: 'column',
  },
  imageFiller: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  backgroundImage: {
    height: height * 0.3,
    // resizeMode: 'cover',
  },
  logo: {
    position: 'absolute',
    top: 10,
    right: 20,
    height: 100,
    width: 100,
    resizeMode: 'contain',
    borderRadius: 10,
  },
  freebieCard: {
    borderRadius: 10,
    position: 'absolute',
    top: height * 0.2,
    width: '95%',
    alignSelf: 'center',
    padding: 10,
    height: height * 0.2,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  containerCard: {
    borderRadius: 10,
    width: '95%',
    alignSelf: 'center',
    padding: 10,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  availability: {
    alignItems: 'flex-start',
    paddingLeft: 15,
  },
  instructionsCard: {
    borderRadius: 10,
    width: '95%',
    alignSelf: 'center',
    padding: 10,
    marginBottom: 10,
  },
  instructions: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buyCard: {
    // backgroundColor: '#cb212c',
    padding: 15,
  },
});