import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  Button,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import BaseComponent from '../../../bootstrap/BaseComponent';
import { connect } from 'react-redux';
import If from 'react-native-loyalty/src/helpers/If';

class AllRewards extends BaseComponent {

  renderRewardItem = ({ item, index, objKey }) => {
    const { userDetails, themeColor } = this.props;
    const userTotalAvailablePoints = userDetails && userDetails.points || 0;
    const lockCheck = (userTotalAvailablePoints >= parseInt(item.worth));
    const deficitPoints = lockCheck ? 0 : (parseInt(item.worth) - userTotalAvailablePoints);

    return (
      <View key={index} style={styles.itemContainer}>
        <View style={styles.itemImageContainer}>
          <Image
            source={item.image ? { uri: item.image } : require('../../../images/freebie_bg.png')}
            style={styles.itemImage}
          />
        </View>
        <View style={styles.itemTextContainer}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={[styles.itemTitle, { color: themeColor }]}>{item.name}</Text>
            {!objKey ? (<MaterialIcons name={"lock"} style={{ color: '#333', fontSize: 24 }} />) : null}
          </View>
          <Text style={styles.itemDescription}>{item.description}</Text>
          <View style={styles.itemBottomContainer}>
            <View style={styles.itemPointsContainer}>
              <Image source={require('../../../images/points.png')} style={styles.itemPointsImage} />
              <Text style={styles.itemPointsText}>{parseInt(item.worth)} {"Points"}</Text>
            </View>
            <If condition={objKey}>
              <Button
                title={"REDEEM NOW"}
                color={themeColor}
                onPress={() => {
                  this.navigate('FreebiePage', { navigate: this.navigate, item, lockCheck, deficitPoints });
                }}
              />
            </If>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { groupedFreebies, keysToDisplay, themeColor } = this.props;
    return (
      <View style={styles.container}>
        {keysToDisplay.map((objKey, index) => {
          let name = objKey ? "lock-open" : "lock";
          let color = objKey ? "#fff" : themeColor;
          return (
            <View key={index} style={{ marginBottom: 30 }}>
              <View style={styles.rewardsList}>
                <MaterialIcons name={name} style={{ color, fontSize: 24, marginRight: 10 }} />
                <Text style={{ color: '#fff', textAlign: 'center', fontWeight: 'bold', fontSize: 18 }}>
                  {objKey ? 'Unlocked Rewards' : 'Upcoming Rewards'}
                </Text>
              </View>
              <Text style={{ color: '#333', fontSize: 16, textAlign: 'center' }}>
                {objKey ?
                  'Enjoy with no cost, here are your Rewards!' :
                  'Unlock these Rewards by Earning Points (OR) Spending'
                }
              </Text>
              <View style={{ marginTop: 5 }}>
                <FlatList
                  data={groupedFreebies[objKey]}
                  keyExtractor={item => String(item.id)}
                  renderItem={({ item, index }) => this.renderRewardItem({ item, index, objKey })}
                />
              </View>
            </View>
          );
        })}
      </View>
    );
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';
    const freebies = state.freebies.freebies;
    const userDetails = state.loyalty.userDetails;
    const typeFreebies = freebies && freebies[props.type] && freebies[props.type].length && freebies[props.type] || [];
    const groupedFreebies = _.groupBy(typeFreebies, (item) => (userDetails && userDetails.points || 0) >= parseInt(item.worth));
    const keysToDisplay = Object.keys(groupedFreebies);

    return {
      themeColor,
      userDetails,
      groupedFreebies,
      keysToDisplay,
    }
  },
  {

  }
)(AllRewards);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: '#fff',
  },
  rewardsList: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#000',
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  itemContainer: {
    flex: 1,
    margin: 10,
    borderRadius: 10,
    flexDirection: 'row',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#ccc',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemImageContainer: {
    flex: 0.3,
    height: 100,
    width: 100,
    borderRadius: 10,
    marginRight: 7,
    overflow: 'hidden'
  },
  itemImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  },
  itemTextContainer: {
    flex: 0.7,
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginRight: 7
  },
  itemTitle: {
    // color: '#cb212c',
    fontSize: 16,
    fontWeight: 'bold'
  },
  itemDescription: {
    flex: 1,
    fontSize: 14,
    fontWeight: 'bold'
  },
  itemBottomContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  itemPointsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  itemPointsImage: {
    marginRight: 5,
    width: 20,
    height: 20,
    resizeMode: 'cover'
  },
  itemPointsText: {
    flex: 1,
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold'
  },
});