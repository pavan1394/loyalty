import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import BaseComponent from '../../../bootstrap/BaseComponent';
import { connect } from 'react-redux';

class YearlyFreebies extends BaseComponent {

  renderItem = ({ item, index }) => {
    const { userDetails, themeColor } = this.props;
    const userTotalAvailablePoints = userDetails && userDetails.points || 0;
    const lockCheck = (userTotalAvailablePoints >= parseInt(item.worth));
    const deficitPoints = lockCheck ? 0 : (parseInt(item.worth) - userTotalAvailablePoints);
    const birthdayCheck = userDetails && userDetails.birthday || null;
    const anniversaryCheck = userDetails && userDetails.anniversary || null;

    return (
      <TouchableOpacity
        onPress={() => {
          // this.props.setFreebieId(item.id, 'yearly');
          this.navigate(
            (birthdayCheck && anniversaryCheck) ? 'FreebiePage' : 'CustomerDetails',
            {
              navigate: this.navigate,
              item,
              lockCheck,
              deficitPoints,
              themeColor
            });
        }}
        style={styles.ybContainer}>
        <View style={[styles.ybImageContainer, { backgroundColor: themeColor }]}>
          <Image
            source={item.image ? { uri: item.image } : require('../../../images/birthday.png')}
            style={styles.ybImage}
          />
        </View>
        <View style={styles.ybBoadyContainer}>
          <Text style={[styles.ybTitle, { color: themeColor }]}>{item.title}</Text>
          <Text style={styles.ybBody}>{item.body}</Text>
        </View>
        <MaterialIcons name="keyboard-arrow-right" style={[styles.ybArrowIcon, { color: themeColor }]} />
      </TouchableOpacity>
    );
  }

  render() {
    const { yearlyFreebies, themeColor } = this.props;
    if (yearlyFreebies.length) {
      return (
        <View style={styles.fill}>
          <Image
            source={require('../../../images/exclusive_rewards.png')}
            style={[styles.exclusiveRewards, { tintColor: themeColor }]}
          />
          <FlatList
            data={yearlyFreebies}
            keyExtractor={(item, index) => item.id || String(index)}
            renderItem={this.renderItem}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';
    const freebies = state.freebies.freebies;
    const yearlyFreebies = freebies && freebies["yearly"] && freebies["yearly"].length && freebies["yearly"] || [];

    return {
      themeColor,
      yearlyFreebies,
      userDetails: state.loyalty.userDetails,
    };
  },
  {

  }
)(YearlyFreebies);

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    margin: 10,
    marginTop: 30,
  },
  exclusiveRewards: {
    height: 60,
    width: 160,
    resizeMode: 'contain',
    marginVertical: 10,
    alignSelf: 'center',
    // tintColor: '#cb212c',
  },
  ybContainer: {
    flex: 1,
    marginBottom: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#ccc',
    padding: 5,
    borderWidth: 1
  },
  ybImageContainer: {
    flex: 0.2,
    height: 100,
    // backgroundColor: '#cb212c',
    padding: 20,
    borderRadius: 10,
    marginRight: 7
  },
  ybImage: {
    flex: 1,
    alignSelf: 'stretch',
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  ybBoadyContainer: {
    flex: 0.8,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: 7,
  },
  ybTitle: {
    // color: '#cb212c',
    fontSize: 16,
    fontWeight: 'bold',
  },
  ybBody: {
    color: '#333',
    fontSize: 12,
    fontWeight: 'bold',
  },
  ybArrowIcon: {
    // color: '#cb212c',
    fontSize: 30
  },
  raised: {
    elevation: 2,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 1.2,
  },
});