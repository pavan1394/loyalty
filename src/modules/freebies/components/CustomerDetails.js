import React from 'react';
import {
  TextInput,
  View,
  Text,
  TouchableHighlight,
} from 'react-native';
import {
  Content,
  Card,
  Form,
  Input,
} from 'native-base';
import BaseComponent from '../../../bootstrap/BaseComponent';

const styles = {
  title: {
    color: '#000',
    fontWeight: 'bold',
    padding: 5,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: '#ddd',
    paddingHorizontal: 5,
    borderRadius: 10,
  },
  buttonContainer: {
    padding: 5,
    width: '100%',
    justifyContent: 'flex-end',
  },
  button: {
    padding: 17,
    borderRadius: 10,
  }
};

export default class CustomerDetails extends BaseComponent {
  render() {
    const { themeColor } = this.props;

    return (
      <Content style={{ backgroundColor: '#fff', padding: 15 }} keyboardShouldPersistTaps="handled">
        <Form>
          <View style={{ padding: 5 }}>
            <Text style={styles.title}>Name</Text>
            <View style={styles.inputContainer}>
              <Input
                selectionColor={themeColor}
                value={this.props.name}
                onChangeText={this.props.setName}
              />
            </View>
          </View>

          <View style={{ padding: 5 }}>
            <Text style={styles.title}>Date of Birth</Text>
            <View style={styles.inputContainer}>
              <Input selectionColor={themeColor}
                keyboardType="email-address"
                value={this.props.email}
                onChangeText={this.props.setEmail} />
            </View>
          </View>

          <View style={{ padding: 5 }}>
            <Text style={styles.title}>Anniversary</Text>
            <View style={styles.inputContainer}>
              <TextInput
                selectionColor={themeColor}
                value={this.props.notes}
                onChangeText={this.props.setNotes}
                multiline
                blurOnSubmit={false}
                underlineColorAndroid="transparent"
              />
            </View>
          </View>

          <View style={styles.buttonContainer}>
            <TouchableHighlight
              style={[styles.button, { backgroundColor: themeColor }]}
              onPress={() => this.save()}
            >
              <Text style={{ color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>SAVE</Text>
            </TouchableHighlight>
          </View>

        </Form>
      </Content>
    );
  }
}