import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import BaseComponent from '../../../bootstrap/BaseComponent';
import { connect } from 'react-redux';
import { setFreebieId } from '../freebies';
import If from 'react-native-loyalty/src/helpers/If';

class MonthlyFreebies extends BaseComponent {

  renderItem = ({ item, index }) => {
    const { userDetails, themeColor } = this.props;
    const userTotalAvailablePoints = userDetails && userDetails.points || 0;
    const lockCheck = (userTotalAvailablePoints >= parseInt(item.worth));
    const deficitPoints = lockCheck ? 0 : (parseInt(item.worth) - userTotalAvailablePoints);

    return (
      <View key={item.id} style={styles.mbRenderItemContainer}>
        <View style={styles.mbItemContainer}>
          <View style={styles.mbItemImageContainer}>
            <Image
              source={item.image ? { uri: item.image } : require('../../../images/freebie_bg.png')}
              style={styles.mbItemImage}
            />
          </View>
          <View style={styles.mbItemTextContainer}>
            <Text style={[styles.mbItemTitle, { color: themeColor }]}>{item.name}</Text>
            <Text style={styles.mbItemBody}>{item.description}</Text>
            <View style={styles.mbButton}>
              <If condition={this.props.freebieIds.includes(item.id)}>
                <Button
                  color={themeColor}
                  title={'CLAIM NOW'}
                  onPress={() => {
                    this.props.setFreebieId(item.id, 'monthly');
                    this.navigate('FreebiePage', { navigate: this.navigate, item, lockCheck, deficitPoints });
                  }}
                />
              </If>
              <If condition={!(this.props.freebieIds.includes(item.id))}>
                <Image source={require('../../../images/claimed.png')} style={styles.mbClaimedImage} />
              </If>
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { monthlyFreebies, themeColor } = this.props;
    if (monthlyFreebies.length) {
      return (
        <View style={[styles.fill, { backgroundColor: themeColor }]}>
          <Text style={styles.mbTitle}>MONTHLY BENEFITS</Text>
          <FlatList
            data={monthlyFreebies}
            keyExtractor={(item, index) => item.id || String(index)}
            renderItem={this.renderItem}
          />
          <TouchableOpacity
            style={styles.mbviewAllBtnContainer}
            onPress={() => {
              this.navigate('AllRewards', { navigate: this.navigate, type: 'monthly' });
            }}
          >
            <Text style={styles.mbviewAllBtnText}>VIEW ALL</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';
    const freebies = state.freebies.freebies;
    const canBuyFreebieIds = state.freebies.canBuyFreebieIds;
    const monthlyFreebies = freebies && freebies["monthly"] && freebies["monthly"].length && freebies["monthly"] || [];
    const freebieIds = canBuyFreebieIds && canBuyFreebieIds["monthly"] && canBuyFreebieIds["monthly"].length && canBuyFreebieIds["monthly"] || [];

    return {
      themeColor,
      monthlyFreebies,
      freebieIds,
      userDetails: state.loyalty.userDetails,
    };
  },
  {
    setFreebieId,
  }
)(MonthlyFreebies)

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    padding: 10,
    margin: 10,
    marginTop: 30,
    borderRadius: 10,
    // backgroundColor: '#cb212c',
  },
  mbTitle: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10,
  },
  mbviewAllBtnContainer: {
    borderRadius: 10,
    backgroundColor: '#fff',
    padding: 10,
  },
  mbviewAllBtnText: {
    color: '#000',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 12,
  },
  mbRenderItemContainer: {
    flex: 1,
    marginBottom: 10,
  },
  mbItemContainer: {
    flex: 1,
    borderRadius: 10,
    flexDirection: 'row',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#ccc',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  mbItemImageContainer: {
    flex: 0.3,
    height: 100,
    width: 100,
    borderRadius: 10,
    marginRight: 7,
    overflow: 'hidden'
  },
  mbItemImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  mbItemTextContainer: {
    flex: 0.7,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginRight: 7,
  },
  mbItemTitle: {
    // color: '#cb212c',
    fontSize: 18,
    fontWeight: 'bold'
  },
  mbItemBody: {
    color: '#000',
    fontSize: 12,
    fontWeight: 'bold'
  },
  mbButton: {
    alignSelf: 'flex-end'
  },
  mbClaimedImage: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
});