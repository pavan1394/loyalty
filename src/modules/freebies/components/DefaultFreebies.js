import React from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import BaseComponent from '../../../bootstrap/BaseComponent';
import { setFreebieId } from '../freebies';

class DefaultFreebies extends BaseComponent {

  renderItem = ({ item, index }) => {
    const { userDetails, themeColor } = this.props;
    const userTotalAvailablePoints = userDetails && userDetails.points || 0;
    const lockCheck = (userTotalAvailablePoints >= parseInt(item.worth));
    const deficitPoints = lockCheck ? 0 : (parseInt(item.worth) - userTotalAvailablePoints);

    return (
      <TouchableOpacity style={styles.freebieWrapper} onPress={() => {
        this.props.setFreebieId(item.id, 'base');
        this.navigate('FreebiePage', { navigate: this.navigate, item, lockCheck, deficitPoints });
      }}>
        <View style={styles.freebieContainer}>
          <ImageBackground
            source={item.image ? { uri: item.image } : require('../../../images/rewards.png')}
            style={styles.imageContainer}
          >
            <View style={styles.bodyContainer}>
              {!lockCheck ?
                (<MaterialIcons name="lock" style={styles.lockIcon} />) :
                (<MaterialIcons name="lock-open" style={styles.lockIcon} />)
              }
              <View style={styles.pointsContainer}>
                <Text style={[styles.pointsNum, { color: themeColor }]}>{parseInt(item.worth)}</Text>
                <Text style={[styles.pointsText, { color: themeColor }]}>Points</Text>
              </View>
              <Text style={styles.itemName} numberOfLines={2}>{item.name}</Text>
            </View>
          </ImageBackground>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { defaultFreebies, themeColor } = this.props;

    if (defaultFreebies.length) {
      return (
        <View style={styles.fill}>
          <View style={styles.headerContainer}>
            <Image style={styles.giftImage} source={require('../../../images/gift.png')} />
            <Text style={styles.freebieHeadingText}>
              YOUR REWARDS & BENEFITS
            </Text>
            <Image style={styles.giftImage} source={require('../../../images/gift.png')} />
          </View>
          <View style={styles.freebieItemList}>
            <MaterialCommunityIcons name={"chevron-left-circle"} style={styles.arrows} />
            <FlatList
              style={styles.flatlistContainer}
              horizontal={true}
              data={defaultFreebies}
              keyExtractor={(item, index) => item.id || String(index)}
              renderItem={this.renderItem}
              showsHorizontalScrollIndicator={true}
            />
            <MaterialCommunityIcons name={"chevron-right-circle"} style={styles.arrows} />
          </View>
          <TouchableOpacity
            style={[styles.raised, styles.viewAllRewardsButton, { backgroundColor: themeColor }]}
            onPress={() => {
              this.navigate('AllRewards', { navigate: this.navigate, type: 'base' });
            }}
          >
            <Text style={styles.viewAllRewardsButtonText}>VIEW ALL REWARDS</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }
}

export default connect(
  (state, props) => {
    const config = state.loyalty.config;
    const themeColor = config && config.themeColor || '#cb212c';
    const freebies = state.freebies.freebies;
    const defaultFreebies = freebies && freebies["base"] && freebies["base"].length && freebies["base"] || [];

    return {
      themeColor,
      defaultFreebies,
      userDetails: state.loyalty.userDetails,
    }
  },
  {
    setFreebieId,
  }
)(DefaultFreebies)

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    margin: 10,
    marginTop: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
  },
  giftImage: {
    height: 30,
    width: 30
  },
  freebieHeadingText: {
    color: '#000',
    paddingHorizontal: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16
  },
  freebieItemList: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  arrows: {
    color: '#000',
    fontSize: 18,
    alignSelf: 'center',
  },
  flatlistContainer: {
    flex: 0.9,
    alignSelf: 'center',
    margin: 5,
  },
  viewAllRewardsButton: {
    borderRadius: 10,
    // backgroundColor: '#cb212c',
    padding: 10,
  },
  viewAllRewardsButtonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 12,
  },
  raised: {
    elevation: 2,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 1.2,
  },
  freebieWrapper: {
    flex: 1,
    paddingVertical: 7,
    marginRight: 7,
  },
  freebieContainer: {
    flex: 1,
    height: 140,
    width: 140,
    borderRadius: 10,
    overflow: 'hidden',
  },
  imageContainer: {
    height: '100%',
    width: '100%',
    // resizeMode: 'contain',
    overflow: 'hidden',
  },
  bodyContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  lockIcon: {
    color: '#FFD700',
    fontSize: 30,
    marginBottom: 5,
  },
  pointsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#fff',
    marginBottom: 5,
    padding: 5,
  },
  pointsNum: {
    fontSize: 13,
    fontWeight: 'bold',
    // color: '#cb212c',
    marginRight: 5,
  },
  pointsText: {
    fontSize: 12,
    // color: '#cb212c'
  },
  itemName: {
    fontSize: 13,
    textAlign: 'center',
    color: '#fff',
  },
});