import apiClient from "../../utils/apiClient";
import { Alert } from 'react-native';

const GET_FREEBIES_DATA_START = "Modules/freebies/GET_FREEBIES_DATA_START";
const GET_FREEBIES_DATA_SUCCESS = "Modules/freebies/GET_FREEBIES_DATA_SUCCESS";
const CAN_BUY_FREEBIES_START = "Modules/freebies/CAN_BUY_FREEBIES_START";
const CAN_BUY_FREEBIES_SUCCESS = "Modules/freebies/CAN_BUY_FREEBIES_SUCCESS";
const BUY_FREEBIE_START = "Modules/freebies/BUY_FREEBIE_START";
const BUY_FREEBIE_SUCCESS = "Modules/freebies/BUY_FREEBIE_SUCCESS";
const SET_FREEBIE_ID = "Modules/freebies/SET_FREEBIE_ID";
const ERROR = "Modules/freebies/ERROR";

const getInitialState = () => ({
  freebies: {},
  canBuyFreebieIds: {},
  freebie: null,
  loading: false,
  freebieId: null,
  freebieType: null,
  error: {
    title: '',
    message: '',
  }
});

export const setFreebieId = (freebieId, freebieType) => ({
  type: SET_FREEBIE_ID,
  freebieId,
  freebieType,
});

export const getFreebiesData = (freebieType) => async (dispatch, getState) => {
  dispatch(getCanBuyFreebiesData(freebieType));
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, loyaltyId } = state.home;
  const { loyaltyCardId } = state.loyalty;

  let url = apiClient.Urls.loyalty + loyaltyId + '/cards/' + loyaltyCardId + '/freebies/' + freebieType;

  dispatch({
    type: GET_FREEBIES_DATA_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
      merchantId,
      loyaltyId,
    });
    console.log('getFreebiesData', freebieType, response);
    if (response.success) {
      dispatch({
        type: GET_FREEBIES_DATA_SUCCESS,
        freebies: response.freebies,
        freebieType,
      });
    } else {
      dispatch(displayError('', response.message));
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', response.message));
  }
};

export const getCanBuyFreebiesData = (freebieType) => async (dispatch, getState) => {
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, loyaltyId } = state.home;
  const { loyaltyCardId } = state.loyalty;

  let url = apiClient.Urls.loyalty + loyaltyId + '/cards/' + loyaltyCardId + '/freebies/' + freebieType + '/can-buy';

  dispatch({
    type: CAN_BUY_FREEBIES_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
      merchantId,
      loyaltyId,
    });
    console.log('canBuy', response);
    if (response.success) {
      dispatch({
        type: CAN_BUY_FREEBIES_SUCCESS,
        canBuyFreebieIds: response.freebieIds,
        freebieType,
      });
    } else {
      dispatch(displayError('', response.message));
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', response.message));
  }
};

export const buyFreebies = (goToPaymentGateway) => async (dispatch, getState) => {
  const state = getState();
  const { authToken, merchantAccessToken } = state.session;
  const { merchantId, loyaltyId } = state.home;
  const { loyaltyCardId } = state.loyalty;
  const { freebieId, freebieType } = state.freebies;

  let url = apiClient.Urls.loyalty + loyaltyId + '/cards/' + loyaltyCardId + '/freebies/' + freebieType + '/' + freebieId + '/buy';

  dispatch({
    type: BUY_FREEBIE_START,
  });

  try {
    const response = await apiClient.post(url, {
      authToken,
      merchantAccessToken,
      merchantId,
      loyaltyId,
    });
    console.log('getFreebiesData', freebieType, response);
    if (response.success) {
      if (response.goToPg) {
        const params = {
          url: response.response.url,
          method: response.response.method || "POST",
          params: response.response.params,
        }
        goToPaymentGateway(params);
      }
      dispatch({
        type: BUY_FREEBIE_SUCCESS,
        freebie: response.freebie,
        freebieType,
      });
    } else {
      dispatch(displayError('', response.message));
      Alert.alert('', response.message);
    }
  } catch (e) {
    console.log("EXCEPTION", e.message);
    dispatch(displayError('EXCEPTION', response.message));
    Alert.alert('', e.message);
  }
};

const displayError = (title, message, loading = 'loading') => ({
  type: ERROR,
  title,
  message,
  loading,
});


export default freebiesReducer = (state = getInitialState(), action) => {
  switch (action.type) {
    case ERROR: {
      return {
        ...state,
        [action.loading]: false,
        error: {
          title: action.title,
          message: action.message,
        }
      }
    }
    case GET_FREEBIES_DATA_START: {
      return {
        ...state,
        errorMessage: '',
        loading: true,
      }
    }
    case GET_FREEBIES_DATA_SUCCESS: {
      return {
        ...state,
        loading: false,
        freebies: {
          ...state.freebies,
          [action.freebieType]: action.freebies,
        },
      }
    }
    case CAN_BUY_FREEBIES_START: {
      return {
        ...state,
        errorMessage: '',
        loading: true,
      }
    }
    case CAN_BUY_FREEBIES_SUCCESS: {
      return {
        ...state,
        loading: false,
        canBuyFreebieIds: {
          ...state.canBuyFreebieIds,
          [action.freebieType]: action.canBuyFreebieIds,
        },
      }
    }
    case BUY_FREEBIE_START: {
      return {
        ...state,
        errorMessage: '',
        loading: true,
      }
    }
    case BUY_FREEBIE_SUCCESS: {
      return {
        ...state,
        loading: false,
        freebie: action.freebie
      }
    }
    case SET_FREEBIE_ID: {
      return {
        ...state,
        loading: false,
        freebieId: action.freebieId,
        freebieType: action.freebieType,
      }
    }
    default:
      return state;
  }
};